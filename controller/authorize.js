const axios = require('axios');
var UserProfile = require('../models/UserProfile')
const moment = require('moment');
const { generateAuthToken } = require('../middlewares/auth');
module.exports.logout = async (req, res) => {
	res.status(200);
	let id = req.userId;
	token = req.token;
	const user = await UserProfile.findById(id);
	let tokens = user.tokens;
	user.tokens = tokens.filter(item => item !== token);
	await user.save();
	res.send({
		message: "Log out successfully",
		userId: id
	})
};
module.exports.login = (req, res) => {
	res.render('auth/login');
};
module.exports.loginP = async (req, res) => {
	const data = req.body;
	console.log(data.type + "dataaaaaaa000");
	if (data) {
		if (data.type === 'facebook_login') {
			try {
				const dataFb = await axios.get(`https://graph.facebook.com/me?fields=email,name,gender,link&access_token=${data.accessToken}`);
				console.log("Create new user scope !");

				if (dataFb && dataFb.data && !dataFb.data.error) {
					console.log("Create new user scope !");

					const profileExists = await UserProfile.findOne({ userId: dataFb.data.id });
					let userId;

					if (profileExists) {
						const userFind = await UserProfile.findById(profileExists._id);
						userId = userFind._id;
					} else {
						console.log("Create new user !");
						const user = await UserProfile.create({ userId: dataFb.data.id + '', type: 'facebook', profile: dataFb.data });
						userId = user._id;
					}
					const token = await generateAuthToken(userId);

					console.log('login user');
					//const token = "923ruh08cyweuhr3hf0seh0fyq3dashdbq2wyi";
					res.status(200);
					res.send({
						message: "Log in successfully",
						token: token,
						userId: userId
					})
				} else {
					res.status(500);
					res.send({
						message: "Log in fail, FB access token was not suitable",
					})
				}
			} catch (error) {
				console.log('getFb data err', error);
				res.status(500);
				res.send({
					message: "Log in fail, FB access token was not valid",
				})
			}
		}
	}
}
module.exports.testToken = (req, res) => {
	res.status(200);
	res.send({
		message: "Test successfully"
	})
};