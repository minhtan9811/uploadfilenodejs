const axios = require('axios');
var UserProfile = require('../models/UserProfile');
var ImageStored = require('../models/ImageStored');
const Resize = require('../Resize');
const path = require('path');
const moment = require('moment');
module.exports.upload = async (req, res) => {
	var userId = req.userId;
	console.log(req.body);
	console.log(req.params);
	// folder upload
	var src = __dirname.replace('routes', '');
	src = __dirname.replace('\controller', '');
	const imagePath = path.join(src, 'public/images');
	// call class Resize
	let timestamp = moment();
	let fname = userId + "-" + timestamp;
	const fileUpload = new Resize(imagePath, fname);
	if (!req.file) {
		res.status(401).json({ error: 'Please provide an image' });
	}
	const filename = await fileUpload.save(req.file.buffer);
	fullPath = process.env.URL + '/' + 'images/' + filename;
	console.log(fullPath);

	// create in Mongo
	try {
		const image = await ImageStored.create({ userId: userId, createdAt: timestamp, fileName: fname });

	} catch (error) {
		return res.status(400).json({ name: "Cannot save image to server", error: error });

	}
	return res.status(200).json({ name: fullPath });






};