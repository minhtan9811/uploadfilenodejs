var express = require('express')
var router = express.Router();
const moment = require('moment');
const { generateAuthToken } = require('../middlewares/auth');
const { UserProfile } = require('../models/UserProfile')
var authMiddleware = require('../middlewares/auth');
var controller = require('../controller/authorize');

router.post('/login', controller.loginP);
router.post('/logout', authMiddleware.auth, controller.logout);
router.get('/login', async (req, res) => {
	console.log("return login views");
	res.render('login');
});


module.exports = router;
