//router.js
const express = require('express');
const app = express();
const controller = require('../controller/uploadImage')
const router = express.Router();
const upload = require('../middlewares/uploadMiddleware');
const authMiddleware = require('../middlewares/auth');
router.get('/', async function (req, res) {
	await res.render('index');
});

router.post('/post', authMiddleware.auth, upload.single('image'), controller.upload);


module.exports = router;