var express = require('express')
var router = express.Router();
const axios = require('axios');
var UserProfile = require('../models/UserProfile')
const moment = require('moment');
const authMiddleware = require('../middlewares/auth');
var controller = require('../controller/authorize');

router.get('/login', async (req, res) => {
	console.log("return login views");
	res.render('login');
});


router.post('/token', authMiddleware.auth, controller.testToken);

module.exports = router;
