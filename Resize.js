const { v4: uuidv4 } = require('uuid');

const sharp = require('sharp');

const path = require('path');

class Resize {
	constructor(folder, namestamp) {
		this.folder = folder;
		this.namestamp = namestamp;
	}
	async save(buffer) {
		const filename = this.namestamp + '.png';
		const filepath = this.filepath(filename);

		await sharp(buffer)
			.resize(600, 600, { // size image 300x300
				fit: sharp.fit.inside,
				withoutEnlargement: true
			})
			.toFile(filepath);

		return filename;
	}
	static filename() {
		// random file name
		return this.namestamp + '.png';
	}
	filepath(filename) {
		return path.resolve(`${this.folder}/${filename}`)
	}
}
module.exports = Resize;