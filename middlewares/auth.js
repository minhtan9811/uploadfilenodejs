const jwt = require('jsonwebtoken')
const config = require('../config')
var express = require('express')
var router = express.Router();
const SIGN_KEY = config.SIGN_KEY;
var UserProfile = require('../models/UserProfile')
module.exports.auth = async (req, res, next) => {
	const token = req.headers['Authorization'] || req.headers['authorization'];
	if (!token) return res.status(401).send({ error: 'Not authorized' });
	try {
		const data = jwt.verify(token, SIGN_KEY);
		// check login or not
		const user = await UserProfile.findOne({ _id: data._id, 'tokens.token': token });
		if (!user) return res.status(401).send({ error: 'Not authorized to access this resource' });
		req.user = "tmp_Closed";
		req.token = token;
		req.userId = user._id;
		next();
	} catch (error) {
		return res.status(401).send({ error: 'Not authorized to access this resource' });
	}
}

module.exports.generateAuthToken = async function (id) {
	// Generate an auth token for the user
	console.log('find by id: ' + id);
	const user = await UserProfile.findById(id);
	const token = jwt.sign({ _id: id }, SIGN_KEY, { expiresIn: '1h' })
	//user.tokens = user.tokens.concat({ token })
	user.tokens = user.tokens.concat({ token })
	await user.save()
	return token
};

