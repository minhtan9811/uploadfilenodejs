const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const imageSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: true,
	},
	createdAt: String,
	fileName: String
}, { versionKey: false });

const ImageStored = mongoose.model('image_stored', imageSchema);
module.exports = ImageStored;
