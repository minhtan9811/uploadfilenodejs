const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

const userProfileSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: true,
	},
	type: String,
	profile: Object,
	tokens: [{
		token: {
			type: String,
		}
	}]
}, { versionKey: false });

const UserProfile = mongoose.model('user_profile', userProfileSchema);
module.exports = UserProfile;
