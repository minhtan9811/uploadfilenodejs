const express = require('express');
const app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
require('dotenv').config();
var cors = require('cors')
app.use(cors());
const port = process.env.PORT || 3000;

// Connect with Mongo
const uri = process.env.COMPASS_URI;
const mongoose = require('mongoose');
mongoose.connect(uri, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true }
);

const connection = mongoose.connection;
connection.once('open', () => {
	console.log("MongoDB successfully");
})


// for route
const router = require('./routes/router');
const authRoute = require('./routes/auth');
const testRoute = require('./routes/test');
app.use('/test', testRoute);
app.use('/upload', router);
app.use('/auth', authRoute);

// for static file in folder public 
app.use(express.static('public'));
app.set('view engine', 'ejs');

app.listen(port, function () {
	console.log('Server is running on PORT', port);
});